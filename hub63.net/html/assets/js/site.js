var _rainDropDensity = 0.5;

window.onload = (e)=> {
    var densitySpreadCount = parseInt(window.innerWidth * _rainDropDensity);

    dropRain(densitySpreadCount);
}

var cards = [
  {
    imageUrl :"assets/imgs/blog.png",
    forSectionSelector :".blog-section",
    margin : '-5px -40px 0px 0px',
    zIndex : "8",
    isActive : false
  },
  {
    imageUrl :"assets/imgs/join.png",
    forSectionSelector :".join-us-section",
    margin : '-20px -40px 0px 0px',
    zIndex : "9",
    isActive : false
  },
  {
    imageUrl :"assets/imgs/blog.png",
    forSectionSelector :".about-us-section",
    margin : '-35px 0px 0px 0px',
    zIndex : "10",
    isActive : true
  },
  {
    imageUrl :"assets/imgs/blog.png",
    forSectionSelector :".blog-section",
    margin : '-20px 0px 0px -40px',
    zIndex : "9",
    isActive : false
  },
  {
    imageUrl :"assets/imgs/join.png",
    forSectionSelector :".join-us-section",
    margin : '-5px 0px 0px -40px',
    zIndex : "8",
    isActive : false
  }
]

$((e)=>{
  fillBottomSliderCards(".bottom-card-holder",cards);
  $(".card").on('click',(e)=>{
    $(".card-active").removeClass('card-active');
    $(e.currentTarget).addClass('card-active');
    if(e.currentTarget.getAttribute('data-target-section')){
      $(`.card-active-section`).removeClass('card-active-section')
      $(`${e.currentTarget.getAttribute('data-target-section')}`).addClass('card-active-section')
    }
  });
  
});

function fillBottomSliderCards(holder,cards){
  console.log(cards);

  for(var i =0; i<cards.length;i++) {

    var _c = cards[i];

    var _cardElement = createBottomSlideCardElement(_c);

    if(_c.isActive)
      $(_cardElement).addClass("card-active");
      
    $(holder).append(_cardElement);
  }
}

function createBottomSlideCardElement(cardInfo){
  return $('<div></div>')
    .addClass('card col')
      .append($('<img/>')
              .addClass('card-img-top')
              .attr("src",cardInfo.imageUrl))
      .attr('data-target-section',cardInfo.forSectionSelector)
      .css('margin',cardInfo.margin)
      .css('z-index',cardInfo.zIndex);
}