
/**
 * starts droping rains with 100ms interval
 * @param {int} countToCreate 
 * @param {int} interval
 */
function dropRain(countToCreate,interval = 100){
    if(countToCreate === 0)
        return

    setTimeout(()=>{
        createAndAppendDrop();
        dropRain(countToCreate - 1);
    },interval)
}

/**
 * Creates rain drop and appends it to the holder
 * @param {string} holder 
 */
function createAndAppendDrop(holder = '.rain-drop-holder'){

    var widthAndHeight = parseInt(Math.random() * 10) + 1;

    var drop = createRainDrop(widthAndHeight,widthAndHeight);

    drop.style.left = `${(Math.random() * 100) + 1}%`;

    $(holder).append(drop)
}

/**
 * Creates a rain drop circle 
 * @param {int} width 
 * @param {int} height 
 */
function createRainDrop(width,height) {

    var drop = document.createElement('span');

    drop.classList.add('rain-drop');
    drop.classList.add('rain-drop-animate');
    drop.style.backgroundColor = getRandomColor();

    drop.style.width = width;
    drop.style.height = height;

    return drop;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  